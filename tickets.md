# Tickets
<p class="tip">Once you have created a ticket please be patient as a staff member will reply as soon as possible.</p>

Enter the command `~new-ticket` in the `#help-requests` channel on our discord.  
***DO NOT PUT YOUR MESSAGE IN THE COMMAND***

A new channel will be created at the top of our discord that belongs to your ticket.

The bot will ping you in the ticket channel asking you to explain why you created the ticket.

Once you've done that a member of staff will respond when they can.