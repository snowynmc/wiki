<center>
  <h2 style="color:red">NOTE TO ALL PLAYERS</h2>

  <p style="color:red;font-weight:bold">The Modded Server will not be moderated. </p>

  All players will however still be expected to follow SnowynMC's [General Rules](rules/general).

  **We are running version 1.0.3 of Mech & Magic LITE**

  This is a whitelisted server, to be added to the whitelist you must [Confirm Your Minecraft Name](confirm-minecraft-name) on the [SnowynMC Discord](https://snowynmc.ga/discord).

  The server IP address is `mm.snowynmc.ga`.

  Any disputes that cannot be resolved will result in all involved parties being removed from the whitelist until such time as the dispute is resolved.

  To protect players builds and possessions the modpack includes a teaming, land claiming and chunk loading mod.  
  [Learn more about chuck claiming here.](https://minecraft.curseforge.com/projects/ftb-utilities/pages/claimed-chunks)

  You can set up to 5 homes `/sethome [name]` and request to teleport to other players with `/tpa [name]`.

  <p style="color:red;font-weight:bold">ONLY ADD PLAYERS YOU TRUST TO YOUR CLAIM.</p>
</center>