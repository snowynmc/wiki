const express = require('express');
const fs = require('fs');
const path = require('path');

console.log('Starting up express app..');
require('dotenv').config();
const port = process.env.HTTP_PORT || 3000;
const app = express();

app.get('*', function (req, res) {
    const index = path.join(__dirname, 'index.html');
    const file = path.join(__dirname, req.url);
    if (req.url.includes('media') || req.xhr) {
        if (fs.existsSync(file) == false) {
            return res.sendStatus(404);
        }
        return res.sendFile(file);
    }
    return res.sendFile(index);
});

app.listen(port);
console.log('Server is now listening..');