- Rules

    - [General](rules/general)
    - [Towny](rules/towny)
    - [Discord](rules/discord)

- General

    - [Confirm Minecraft Name](confirm-minecraft-name)
    - [Help Tickets](tickets)

- Hub

    - [General](hub/general)
    - [Towny](hub/towny)
    - [SkyBlock](hub/skyblock)
    - [Vanilla](hub/vanilla)

- Modded

    - [Modded Server](modded)