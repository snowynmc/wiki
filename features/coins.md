# Coins
We are introducing a new system that replaces the Towny Voting which are virtual coins used in the hub.  
Now when you vote you will receive coins on the hub instead of getting a Towny Crate.

If you purchase a crate or convert to currency you will receive it after you switch to the server.  
You will get a virtual key not a physical item for crates which can be used in the servers spawn area on the crate boxes.

### How To Get Coins
- **Voting** - 15 Coins
- **Play 1 Hour** - 5 Coins
- **Login Each Day** - 5 Coins
- **Vote Party** - 100 Coins

### What You Can Buy
- **Mystery Box** - 25 Coins
- **Towny Basic Crate** - 50 Coins
- **Towny Super Crate** - 1500 Coins
- **SkyBlock Basic Crate** - 50 Coins
- **SkyBlock Super Crate** - 2500 Coins

### Convert Coins To Currency
**Towny** has a conversion rate of **1 Coin** to **$7**.

**SkyBlock** has a conversion rate of **1 Coin** to **$14**.  