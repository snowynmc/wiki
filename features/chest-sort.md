# Chest Sorting

Type `/chestsort` to toggle chest sorting.

When you have chest sorting on it will sort the following when you close them.
- Regular Chests
- Shulker Boxes
- Ender Chest

You can sort your player inventory with the commands below.
- **invsort** - Sort the inventory.
- **invsort hotbar** - Sort the hotbar.
- **invsort all** - Sort the invetory and hotbar.