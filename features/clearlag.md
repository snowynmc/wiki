# ClearLag

ClearLag is a plugin we use to help prevent lag caused by mass amounts of entities.

**What does is clear?**  
Every 30 minutes it clears the following..

- Falling Blocks
- Ground Items
- Experience Orbs
- Unoccupied Boats

It also removes every mob except the following..

- Elder Guardian
- Ender Dragon
- Ghast
- Guardian
- Iron Golem
- Shulker
- Stray
- Vex
- Villager
- Wither

**How do I stop it from removing my animals?**

- You can tame the animals
- You can nametag the animals or mobs
- You can put chickens above hoppers which acts as an egg farm

**What else does it do?**  
It prevents each chunk from having no more then 25 entities in them.