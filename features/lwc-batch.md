# LWC Batch Mode

Tired of having to unlock or add your friends to every chest?  
Now you can do so with one command.

Enter `/lwcbatch toggle` to toggle batch mode.  
You cannot open chests when you have batch mode on.

You can have up to **20 single or double chests** in your selection.

Left clicking a chest adds it to the selection or right click to remove the chest.

Enter `/lwcbatch` to see the commands you can run.  
Now you run any of the commands on your selection.

The cmodify subcommand works the same as the regular LWC command as [described on their wiki](https://github.com/Tsuser1/Modern-LWC/wiki/Getting-Started).

<img src="https://i.ibb.co/W2mT4dC/lwc-batch-try.png">

This would add **Chinz** and remove **CrappyWebHost** from the access list on the chests you have selected.