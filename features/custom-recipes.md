# Custom Recipes

**Leather**  
<img src="https://i.ibb.co/bmJgS5n/recipe-leather.png">

**Saddle**  
<img src="https://i.ibb.co/dLRxV0d/recipe-saddle.png">

**Horse Armor**  
You can replace the iron with gold or diamond to get those horse armor as well.  
<img src="https://i.ibb.co/y8fgdHN/recipe-horse-armor.png">

**Gravel**  
<img src="https://i.ibb.co/bRdKDKS/recipe-gravel.png">

**Nether Wart Block ⇢ Nether Wart**  
<img src="https://i.ibb.co/vPF3QtS/recipe-nether-wart-block.png">

**Blue Ice ⇢ Packed Ice**  
<img src="https://i.ibb.co/VqDc3Zr/recipe-blue-ice.png">

**Packed Ice ⇢ Ice**  
<img src="https://i.ibb.co/gvX5Y9w/recipe-packed-ice.png">

**Smooth Stone**  
<img src="https://i.ibb.co/h8j54pm/recipe-smooth-stone.png">