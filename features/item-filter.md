# Item Filter

**Towny Only:** You can purchase this from the Perk Shop.

You can have up to **14 items** in the item filter.

Use `/itemfilter` to open the menu.

Click an item in your inventory to add it to the filter.  
Click an item in the filter menu to remove it from the filter.