# Sleep Vote

Anyone can start a sleep vote with `/sleepvote`  
When there is a sleep vote going you can vote with the same command or enter a bed.

You can only start a vote when it is night time in-game.  
You can only start a vote in Main and Resource World.

<img src="https://i.ibb.co/ctxdH7s/sleep-vote.png">