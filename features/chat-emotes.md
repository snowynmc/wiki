# Chat Emotes

**Text Emotes**
```
:shrug: = ¯\_(ツ)_/¯
:fliptable: = (╯°□°）╯︵┻━┻
:unfliptable: = ┬─┬◡ﾉ(°-°ﾉ)
:disapprove: = ಠ_ಠ
:happy: = ◠‿◠
:happy2: = ◕‿◕
:happy3: = ʘ‿ʘ
:cat: = ฅ^•ᴗ•^ฅ
:creep: = (°ᴗ°)
:what: = (☉_☉)
:sleep: = (ᴗ_ᴗ)
:facepalm: = (－_ლ)
:why: = ლ(ಠ_ಠლ)
:angry: = (◣_◢)
:mustache: = (ˇ෴ˇ)
:cantunsee: = ♨_♨
:party: = ٩(ᐛ)و
```

**Unicode Emotes**
```
:star: = ★
:note: = ♪
:note2: = ♫
:oheart: = ♡
:heart: = ❤
:pencil: = ✎
:tm: = ™
:x: = ✖
:check: = ✔
:snow: = ❄
:mail: = ✉
:peace: = ✌
:plane: = ✈
:skull: = ☠
:hotcup: = ☕
:picks: = ⚒
:swords: = ⚔
```
