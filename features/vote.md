# Voting
Support the server and help us conquer the world by voting on the websites below every day.

Vote to get 5 coins for each website below.  

When 200 votes are registered a vote party is triggered and everyone will recieve 100 coins.

- [minecraft-mp.com](https://minecraft-mp.com/server-s205232)
- [minecraftservers.org](https://minecraftservers.org/vote/514481)
- [minecraft-server.net](https://minecraft-server.net/vote/snowynmc)
- [minecraft-server-list.com](https://minecraft-server-list.com/server/431069/vote)
- [topg.org](https://topg.org/Minecraft/in-498814)
- [planetminecraft.com](https://planetminecraft.com/server/snowynmc/vote)