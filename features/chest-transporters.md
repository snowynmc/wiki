# Chest Transporters

Here's a quick guide on using chest transporters.

- Enter `/ct` to get a chest transporter stick.
- `/unlock` the chest you want to move.
- Left click the chest with the stick to pick it up.
- Right click the transporter stick on a block to place it down.