# Commands

| Command              | Aliases | Description                                             |
| -------------------- | ------- | ------------------------------------------------------- |
| rules                |         | Show our server rules.                                  |
| spawn                |         | Sends your to the server spawn.                         |
| helpop [message]     |         | Send a message to online staff for help.                |
| ignore               |         | Lets you ignore or unignore a player.                   |
| mail                 |         | Allows you to send mail to other players.               |
| realname [user]      |         | Show the username of someones nickname.                 |
| seen [user]          |         | Show how long someone was online or last online.        |
| warp                 |         | Opens the warp menu.                                    |
| home [name]          |         | Teleport to a home.                                     |
| sethome [name]       |         | Sets a home at your location.                           |
| delhome [name]       |         | Deletes a home.                                         |
| kit                  |         | Spawn a predefined kit.                                 |
| afk                  |         | Set your status as AFK.                                 |
| hdb                  |         | Opens the custom heads menu.                            |
| itembox              |         | Allows you to send item mail to other players.          |
| es                   |         | Toggles sign edit mode.                                 |
| perkshop             |         | Opens the perk shop menu.                               |
| auction              | ah      | Opens the auction house menu.                           |
| keys                 |         | Show how many crate keys you have.                      |

# Money Commands
| Command              | Aliases | Description                                             |
| -------------------- | ------- | ------------------------------------------------------- |
| balanace             | bal     | Show your current balance.                              |
| balalancetop         | baltop  | Show the top players in balances.                       |
| pay [user] [amount]  |         | Pay another player.                                     |
| paytoggle            |         | Toggle if you are accepting payments.                   |
| payconfirm           |         | Toggle if you need to confirm payments.                 |

# Teleport Commands
| Command              | Aliases | Description                                             |
| -------------------- | ------- | ------------------------------------------------------- |
| tpa [user]           |         | Asks a player if you can teleport to them.              |
| tpahere [user]       |         | Asks a player if they can teleport to you.              |
| tpaccept             | tpyes   | Accept a TPA request.                                   |
| tpdeny               | tpno    | Denies a TPA request.                                   |
| tptoggle             |         | Toggle if you can receive TPA requests.                 |

# Chat Commands
| Command              | Aliases | Description                                             |
| -------------------- | ------- | ------------------------------------------------------- |
| join [channel]       |         | Join a channel and set it as your active.               |
| leave [channel]      |         | Leave channel.                                          |
| listen [channel]     |         | Join a channel but don't set it as your active.         |
| chlist               |         | Show current available channels.                        |
| chwho [channel]      |         | See who is listening on the channel.                    |
| msg [user] [message] | tell    | Send a private message.                                 |
| reply [message]      | r       | Reply to the last private message.                      |
| notifications        |         | Toggle the sound notification for PMs.                  |
| [channel] [message]  |         | Send a quick message to the channel without joining it. |

# Perk Commands
| Command              | Aliases | Description                                             | Require Perk       |
| -------------------- | ------- | ------------------------------------------------------- | ------------------ |
| autreplant           |         | Toggle autoreplant mode.                                | Autoreplant        |
| fly                  |         | Toggle creative flight.                                 | Creative Flight    |
| tfly                 |         | Toggle town flight.                                     | Town Flight        |
| pv [number]          |         | Open your player vaults.                                | Vault              |
| nick [name]          |         | Set a nickname.                                         | Nick Command       |
| ec                   |         | Open your ender chest.                                  | Ender Chest        |
| xpbottle [amount]    |         | Store XP in a bottle.                                   | XP Bottle Command  |
| top                  |         | Teleport to the highest block at your location.         | Top Command        |
| jump                 |         | Teleport to the block your cursor is pointed.           | Jump Command       |
| condense             |         | Merge similiar items into their respective block form.  | Condense Command   |
| trash                |         | Open the trash menu.                                    | Trash Sign         |
| feed                 |         | Replenishes your food.                                  | Feed Command       |
| heal                 |         | Replenishes your hearts and food.                       | Heal Command       |
| craft                |         | Open a portable crafting table.                         | Craft Command      |
| ptime [time]         |         | Set your personal time.                                 | Player Controls    |
| pweather [weather]   |         | Set your personal weather.                              | Player Controls    |
| hat                  |         | Set the block in your hand as your hat.                 | Hat Command        |
| skull [name]         |         | Get a skull of another player.                          | Skull Command      |
| disenchant           |         | Remove enchants from item your holding.                 | Disenchant         |
| itemfilter           |         | Open the item filter menu.                              | Item Pickup Filter |

