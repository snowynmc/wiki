# Town Warps

Town warps is a free feature available to everyone that owns a town.  
**Each town can have up to 10 warps.**  
Warps can only be managed by the Mayor and Assistants.

**Commands**
- **twarp** - View your town warps.
- **twarp public** - View public town warps.
- **twarp create <name>** - Create a warp.
- **twarp remove <name>** - Remove a warp.
- **twarp removeall** - Remove all your town warps.
- **twarp toggle <name>** - Toggle a warp public to outsiders.