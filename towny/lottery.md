# Lottery

Each ticket costs **$200** and adds an additional **$600** to the pot.

You can purchase tickets with either command below.

```
/lottery buyrandom [amount]
/lottery buy [number] [number] [number] [number]
```

For a ticket to be a winner you need to match the four numbers regardless of position.

Tickets are claimed after the draw happens and when a player connects to the server.

The lottery draws **every 2 hours** and starts reminding an hour before.

At least two players are required to have purchased tickets for the lottery to draw.  
If one person purchased tickets, their tickets will be valid for the next draw.

If the lottery draws and there were no winners the pot will be rolled over to the next draw.

You can view the current lottery status with `/lottery status`