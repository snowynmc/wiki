# Custom Enchantments

| Found    | Name          | Categories | Description                                                                                       | Max Level |
| -------- | ------------- | ---------- | ------------------------------------------------------------------------------------------------- | --------- |
| Helmets  | Glowing       | Legendary  | Gives user night vision                                                                           | 1         |
| Helmets  | Mermaid       | Rare       | Gives user water breathing                                                                        | 3         |
| Boots    | Springs       | Rare       | Gives user a spring boost                                                                         | 5         |
| Boots    | Anti-Gravity  | Legendary  | Gives user a bigger spring boost                                                                  | 3         |
| Boots    | Gears         | Legendary  | Gives user a speed boost                                                                          | 3         |
| Armor    | Implants      | Event      | Slowly feeds user while walking                                                                   | 5         |
| Armor    | Burn Shield   | Event      | Gives user fire resistance                                                                        | 1         |
| Armor    | Overload      | Legendary  | Gives user health boost                                                                           | 5         |
| Armor    | Hulk          | Legendary  | Gives user strength, damage resistance and slowness                                               | 2         |
| Armor    | Ninja         | Event      | Gives user health boost and speed                                                                 | 2         |
| Armor    | Enlightened   | Rare       | Has a chance to heal user when being attacked                                                     | 5         |
| Armor    | Freeze        | Common     | Has a chance to give attacker slowness                                                            | 2         |
| Armor    | Fortify       | Common     | Has a chance to give attacker weakness                                                            | 3         |
| Armor    | Molten        | Common     | Has a chance to set attacker on fire                                                              | 5         |
| Armor    | Pain Giver    | Common     | Has a chance to give attacker poison                                                              | 4         |
| Armor    | Savior        | Legendary  | Has a chance to cut incoming damage in half                                                       | 5         |
| Armor    | Insomnia      | Legendary  | Gives user nausea, mining fatigue and slowness but also gives a high chance to deal double damage | 1         |
| Armor    | Valor         | Rare       | Gives user resistance                                                                             | 2         |
| Armor    | Smoke Bomb    | Rare       | Has a chance to give attacker slowness and blindness                                              | 3         |
| Armor    | Drunk         | Rare       | Gives user strength, mining fatigue and slowness                                                  | 3         |
| Armor    | Voodoo        | Rare       | Has a chance to give attacker weakness                                                            | 3         |
| Armor    | Recover       | Legendary  | Gain regeneration and absorption hearts                                                           | 1         |
| Armor    | Cactus        | Common     | Has a chance to damage attacker                                                                   | 3         |
| Armor    | Storm Caller  | Event      | Has a chance to strike attacker with lightning                                                    | 4         |
| Armor    | Blizzard      | Legendary  | Gives enemies near the user slowness                                                              | 1         |
| Armor    | Intimidate    | Legendary  | Gives enemies near the user weakness                                                              | 2         |
| Armor    | Sand Storm    | Rare       | Has a chance to give nearby players blindess for 10 seconds                                       | 1         |
| Armor    | Acid Rain     | Legendary  | Has a chance to give nearby players poison for 4 seconds                                          | 3         |
| Armor    | Radiant       | Event      | Has a chance to set nearby players on fire                                                        | 3         |
| Armor    | Necromancer   | Event      | Spawns in zombies that fight for one minute                                                       | 3         |
| Armor    | Infestation   | Event      | Spawns in silverfish and endermites that fight for one minute                                     | 2         |
| Bows     | Piercing      | Common     | Has a chance to deal double damage                                                                | 3         |
| Bows     | Doctor        | Rare       | Heals the player that was shot                                                                    | 3         |
| Bows     | Boom          | Legendary  | Has a chance to spawn an explosion where arrow lands                                              | 3         |
| Bows     | Venom         | Rare       | Has a chance to give the player shot poison                                                       | 5         |
| Bows     | Ice Freeze    | Common     | Has a chance to give the player shot slowness                                                     | 1         |
| Bows     | Multi Arrow   | Event      | Has a chance to shoot multiple arrows                                                             | 5         |
| Bows     | Pull          | Legendary  | Has a chance to sent the player shot flying towards the shooter                                   | 3         |
| Axes     | Blessed       | Rare       | Has a chance to remove negative effects from the user                                             | 3         |
| Axes     | Dizzy         | Common     | Has a chance to give the person attacked nausea                                                   | 3         |
| Axes     | Berserk       | Common     | Has a chance to give the user strength and mining fatigue when fighting                           | 3         |
| Axes     | Curse         | Common     | Has a chance to give the person attacked mining fatigue                                           | 3         |
| Axes     | Rekt          | Rare       | Has a chance to dealing double damage                                                             | 3         |
| Swords   | Inquisitive   | Common     | Has a chance of giving more XP                                                                    | 5         |
| Swords   | Vampire       | Common     | Has a chance to healing one heart when attacking                                                  | 5         |
| Swords   | Life Steal    | Rare       | Has a chance to stealing the enemies health                                                       | 5         |
| Swords   | Double Damage | Rare       | Has a chance to dealing double damage                                                             | 3         |
| Swords   | Slow-Mo       | Common     | Has a chance to give the enemy slowness                                                           | 3         |
| Swords   | Blindness     | Common     | Has a chance to give the enemy blindness                                                          | 2         |
| Swords   | Viper         | Common     | Has a chance to give the enemy poison                                                             | 3         |
| Swords   | Fast Swipe    | Common     | Has a chance to deal 33% more damage                                                              | 2         |
| Swords   | Lightweight   | Rare       | Has a chance to give the user haste                                                               | 3         |
| Swords   | Confusion     | Common     | Has a chance to give the enemy nausea                                                             | 4         |
| Swords   | Execute       | Legendary  | Gives the user strength when the enemy is on 2 health                                             | 5         |
| Swords   | Obliterate    | Common     | Has a chance to send the enemy flying backwards                                                   | 3         |
| Swords   | Snare         | Common     | Has a chance to give the enemy slowness and mining fatigue                                        | 3         |
| Swords   | Trap          | Rare       | Has a chance to give the enemy a high dose of slowness                                            | 2         |
| Swords   | Wither        | Rare       | Has a chance to give the enemy wither                                                             | 3         |
| Swords   | Rage          | Legendary  | Does more damage the longer the user fights an enemy                                              | 3         |
| Pickaxes | Auto Smelt    | Legendary  | Has a chance to smelt ore and drop more then one ore                                              | 5         |
| Pickaxes | Experience    | Common     | Has a chance to give the user more XP from block breaking                                         | 3         |
| Pickaxes | Blast         | Event      | Breaks a 3x3 radius                                                                               | 3         |
| Pickaxes | Furnace       | Rare       | Smelts ores into ingots                                                                           | 1         |
| Tools    | Telepathy     | Legendary  | Blocks broken automatically go into the users inventory                                           | 1         |
| Tools    | Haste         | Rare       | Gives the user haste when in hand                                                                 | 3         |
| Tools    | Oxygenate     | Common     | Gives the user water breathing when in hand                                                       | 1         |
| Misc     | FitBit        | Event      | Has a chance to fix the item while the user walks                                                 | 4         |

