# Town Ranks

This is a list of the commands which the different available Towny Ranks have permission to perform in your town.

## Mayor
In addition to everything a Assistant can do players with the Mayor Rank are exempt from taxes and can also.

- Delete their town with `/t delete` but must follow the rules with regards to town deletion
- Create a Nation with `/nation new [name]`
- Promote Residents to the Builder rank


## Assistant
In addition to everything a Helper can do players with the Assistant Rank are exempt from taxes and can also.

- Claim land for the town with `/t claim`
- Unclaim land for the town with `/t unclaim`
- Manage plot perms and toggle town settings with `/town toggle [public|open|explosion|fire|mobs]`
- Build, Break, Switch and Item Use in any town owned plots.
- Promote Residents to Helper and VIP ranks


## Builder
In addition to everything a Resident can do players with the Builder Rank can also.

- Build, Break, Switch and Item Use in any town owned plots.


## Helper
In addition to everything a Resident can do players with the Helper Rank can also.

- Invite players to the town with `/t invite [username]`
- Add and remove plots from sale with..
    - `/plot forsale [price]`
    - `/plot notforsale`


## VIP
Players with the VIP Rank can do everything a Resident can but are exempt from paying taxes.

## Resident
In addition to everything a Nomad can do players with the Resident Rank can also.

- See who is currently online from their town with `/t online`
- Go to your town spawn with `/t spawn`
- Leave the town they are currently a Resident in with `/t leave`
- Add money to the town bank with `/t deposit [amount]`
- Claim a plot that is for sale within town with `/plot claim`
- Claim a plot that you own within town with `/plot unclaim`
- Check the permissions on a plot with `/plot perm`
- Change their plot permissions and toggle plot settings with..
    - `/plot set perm [resident|all|outsider] [build|destroy|switch|itemuse]`
    - `/plot set reset`
    - `/plot toggle [fire|explosion|mob]`


## Nomad
When a player first joins the server they are known as a Nomad, meaning they do not belong to a town and there for have no Towny rank.

- Create a town with `/t create [name]` this costs $5,000
- Join an open town with `/t join [name]`
- Look at the list of all Towns on the server with `/t list`
- Look at the list of all Nations on the server with `/n list`
- Look to see if there are any Towny claimed plots around them with `/towny map` or `/towny map big`
- Look at the time until a new day start with `/town time`
- Look at various town leader boards with `/towny top`
- Check if you are within a town or which town you are in with `/t here`
- Visit public towns with `/t spawn [name]`
- Check the cost of starting a new Town or Nation and the base upkeep cost with `/towny prices`