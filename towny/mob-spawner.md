# Mob Spawner Ritual

We have a neat way of capturing mobs and getting a mob spawner from them.    
You will need **16 Iron Bars** and **1 Emerald**.

**Perform Ritual**

- Surround the mob in a 2 high cage using the iron bars
- Get the mob to at least half health
- Right click the mob with the emerald
- Magic will happen and you might get a spawner in return

If the ritual fails you'll get a message with the failure percent.  
You need to get 10% or less for it to succeed or purchase the Better Chance perk which requires 15%.

**You can do this ritual on these mobs.**

- Chicken
- Cow
- Creeper
- Magma Cube
- Mushroom Cow
- Pig
- Rabbit
- Sheep
- Skeleton
- Slime
- Spider
- Squid
- Wither Skeleton
- Witch
- Zombie
- Zombie Pigman