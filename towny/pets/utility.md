# Utility Skill Tree

**LEVEL 1**  
Has 9 inventory slots

**LEVEL 2**  
Regenerates 2 HP every 45 seconds  
Max health increased to 21.5

**LEVEL 3**  
Has 18 inventory slots

**LEVEL 4**  
Has a 15% chance to poison the target for 3 seconds  
Damage assigned to 1.5  
Can use Duel behavior mode

**LEVEL 5**  
Has 27 inventory slots  
Max health increased to 24.5

**LEVEL 6**  
Can pick up items within a meter

**LEVEL 7**  
Has 36 inventory slots

**LEVEL 8**  
Max health increased to 29.5

**LEVEL 9**  
Can pick up items within 2 meters

**LEVEL 10**  
Has 45 inventory slots