# Combat Skill Tree

**LEVEL 1**  
Damage assigned to 1.5  
Can use Duel behavior mode  

**LEVEL 2**  
Regenerates 1 HP every second  

**LEVEL 3**  
Damage raised to 3.0  
Can use Friendly, Aggressive and Duel behavior modes

**LEVEL 4**  
Max health increased to 22

**LEVEL 5**  
Has a 10% chance to reflect 15% of incoming damage

**LEVEL 6**  
Damage raised to 4.5  
Has 9 inventory slots

**LEVEL 7**  
Can now be controlled with leash

**LEVEL 8**  
Damage raised to 6.0

**LEVEL 9**  
Has a 18% chance to reflect 15% of incoming damage

**LEVEL 10**  
Has a 15% chance to knock back their target