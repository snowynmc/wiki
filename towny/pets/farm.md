# Farm Skill Tree

**LEVEL 1**  
Has 9 inventory slots

**LEVEL 2**  
Can pick up items within a meter

**LEVEL 3**  
Damage assigned to 2.0

**LEVEL 4**  
Can use Friendly, Aggressive, Farm, Raid and Duel behavior modes

**LEVEL 5**  
Has 18 inventory slots  
Damage raised to 2.5

**LEVEL 6**  
Has a 20% chance to reflect 10% of incoming damage

**LEVEL 7**  
Shoot 34 snowballs per minute which causes 2 damage each

**LEVEL 8**  
Can now be controlled with leash

**LEVEL 9**  
Has a 20% chance to poison the target for 2 seconds

**LEVEL 10**  
Can pick up items within 5 meters