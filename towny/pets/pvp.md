# PVP Skill Tree

**LEVEL 1**  
Damage assigned to 1.5

**LEVEL 2**  
Damage raised to 3.0

**LEVEL 3**  
Can use Friendly and Duel behavior modes  
Can now be controlled with leash

**LEVEL 4**  
Can now sprint towards the target  
Has a 10% chance to slow targets down for 2 seconds

**LEVEL 5**  
Has a 15% chance to set enemies on fire for 3 seconds  
Damage raised to 3.5

**LEVEL 6**  
Has 9 inventory slots

**LEVEL 7**  
Strength I beacon for 8 seconds within 5 meters

**LEVEL 8**  
Regenerates 4 HP every second

**LEVEL 9**  
Max health increased to 25  
Has a 15% chance to slow targets down for 3 seconds

**LEVEL 10**  
Strength II, Resistance II and Invisibility beacon for 8 seconds within 5 meters