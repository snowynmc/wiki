# Ride Skill Tree

**LEVEL 1**  
Able to mount your pet by right clicking with lead

**LEVEL 2**  
Shoot 34 snowballs per minute which causes 2 damage each  
Can use Duel behavior mode

**LEVEL 3**  
Has 18 inventory slots

**LEVEL 4**  
Can pick up items within a meter

**LEVEL 5**  
Has 27 inventory slots

**LEVEL 6**  
Can now be controlled with leash

**LEVEL 7**  
Can use Friendly and Duel behavior modes

**LEVEL 8**  
Can now sprint towards the target

**LEVEL 9**  
Has a 50% chance to reflect 20% of incoming damage

**LEVEL 10**  
Regeneration II and Water Breathing beacon for 8 seconds within 10 meters