# How To Get a Pet
It's up to you to find the mob you want and capture it as a pet.  
Once you found the mob you want it must meet it's leash requirements.  
You can see each mobs leash requirement here on the wiki.  
Once your mob has met the leash requirements you just hit your mob with a lead.  
After hitting your mob it will convert from a normal mob into a pet.

## Taking Care of Your Pet
Having a pet is not just a cute shadow that follows you, you need to teach it new skills.  
You need to choose a skill tree in order for your pet to level up.  
Just use the command `/pcst [tree]`.

## Available Skill Trees
- [Combat](towny/pets/combat)
- [PVP](towny/pets/pvp)
- [Ride](towny/pets/ride)
- [Farm](towny/pets/farm)
- [Utility](towny/pets/utility)

## Pet Commands
- **mypet** - Show available pet commands.
- **petinfo** - Show info about your pet.
- **petname [name]** - Set the name of your pet.
- **petrelease [name]** - Release the pet.
- **petcall** - Teleport your pet to you.
- **petsendaway** - Send your pet away. You can call it back later.
- **petswitch** - Allows you to switch between pets.
- **petstore** - Allows you to store your pet. You can store up to 5 pets.
- **petskill** - Shows info about the skills of your pet.
- **petstop** - Orders your pet to stop attacking his target.
- **petinventory** - Opens your pets inventory.
- **petchooseskilltree [tree]** - Select a skill tree for your pet.
- **petpickup** - Toggles if your pet will pickup items.
- **petbeacon** - Opens the beacon window of your pet.
- **petbehavior [mode]** - Toggles the behavior of your pet.
```
friendly - Will not fight even when attacked​
normal - Will fight back when its attacked​
aggressive - Automatically attacks everything within 15 blocks of you​
farm - Automatically attacks every monster within 15 blocks of you​
raid - Like normal but the pet will not attack players​
duel - Will attack other pets that have duel behavior withing 5 blocks
```
