# General Rules

**[Discord](rules/discord) • [Towny](rules/towny)**

**Be Respectful**

* Be nice towards SnowynMC staff and other players.
* Death threats and suicide encouragement will result in a immediate permanent ban from all services.
* Return items that don't belong to you, to their owner, regardless of how they were obtained, unless the owner states you may keep something.
* No inappropriate or offensive skins.
* No inappropriate or offensive builds.
* Do not harass players in any way.
* The use of hate speech, slurs or discrimination towards anyone, even when meant as a joke between friends, is not permitted.
* Do not share private or personal information about others.
* Do not impersonate other players or SnowynMC staff names in any way, such as nicknames.

**Use Chat Responsibly**

* English only in public chat channels.
* No inappropriate nicknames, usernames or named items.
* Mild profanity is permitted but must not be excessive or directed towards others. Attempting to evade the profanity filter could lead to a ban.
* Do not spam chat, be it excessive characters or lots of short messages in quick succession.
* Do not use excessive caps.
* Do not advertise other servers.
* Do not post malicious links.
* Polarizing topics such as politics or religion, even with the intent of a mature debate have no place in public chat channels.
* Drug references are not permitted.

**No Scamming**

* Uphold agreements you have made with others unless that agreement would break any server rules.
* Player casinos or slot machines are permitted but must have clear signs informing the cost, payouts and the chances of each payout.
* Players must not use the discord, forum or any server chat channels to offer monetary transactions in exchange for in-game items.

**No Griefing**

* If you didn't build it, do not add or take away from it without the builders permission in any world.
* Do not take from unlocked chests except when permitted to do so by the owner.
* Do not tamper with redstone circuits, such as adding or remove items from hoppers.
* Do not spawn unwanted animals or mobs within towns or islands.

**No AFK**

<p class="warn">While AFK literally means Away From Keyboard, here we mean, if you are not actively playing on the server, but trying to keep an area chunk loaded by any means that prevents the auto kick from happening, that is breaking this rule.</p>

* Do not attempt to get around the AFK timer by any means.

**No Alternate Accounts or Alts**

* One account per player.
* You are responsible for your account regardless of who was using it.

**No Cheating**

* Do not exploit glitches. If you found glitches report them via our forums.
* Hacking or the use of unapproved mods will result in an immediate ban.
* See list of approved mods here. If there is a mod you would like added to the approved list, submit it for review.

**No traps with the intent to harm another player**

* Any redstone machine.
* An unsafe town spawn or town warps.
* An unsafe island spawn.
* Teleporting a player to an unsafe location such as the edge of a ravine, under water or into lava.
* Making portals unsafe in the end, nether or overworld.
* Requesting a player to teleport to you while you are in a PvP arena.