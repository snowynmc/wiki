# Discord Rules

**In addition to SnowynMC general rules users of the discord must also follow these rules.**

* Please only speak English in voice and text channels.
* Do not use excessive caps or spam.
* Do not post inappropriate links or photos.
* Do not harass or discriminate others.
* Do not impersonate another.
* Do not discuss inflammatory topics.
* Death threats and suicide encouragement will result in a immediate permanent ban from all services.
* Do not play loud music or noises through your microphone.
* Please be considerate of other users and try to keep background noise to a minimum.
* Voice changers are not allowed.