# Towny Rules

<p class="tip">Mining must be done in the resource world.</p>

<p class="tip">Resource gathering must be done in resource or town farms.</p>

**General Rules**

* Mayors may evict a resident after 7 days of inactivity unless they have a pre-existing agreement with that resident stating otherwise.
* If you wish to evict a player from your town you must give them 7 days notice to clear their plots.
* Mayors may set town rules and taxes which must be clearly written at town spawn along with the consequences for breaking said rules.
* Any changes to town rules require a 4 day notice to all residents and may only be enforced after those 4 days.
* Any changes to town taxes require a 4 day notice to all residents prior to the change.
    * Changing a players plot type to one with a higher tax is considered changing taxes.
* Mayors may remove builds at their discretion, but must notify and give the plot owner 2 days to remove their build.
    * After these 2 days mayors may remove the build, minus chests, but must return blocks and items to the plot owner.
    * In the event of the plot owner having left town, or been evicted or banned from town, the blocks and items become property of the town.
    * This does not apply in cases where the player has built outside of their permitted area, inappropriate builds or griefing ― these should be reported to staff.
* Mayors and assistants can use `/clearprotections` to remove chests of inactive players after 7 days. If a player leaves town but remains active on the server, mayors should in the first place ask the former resident to remove the chests, failing this mayors may contact staff to clear the chests for them.
* You must not surround another town as this would prevent them from expanding.
* You cannot claim within 10 chunks of a town.
* Do not harass other towns.
* Mayors may only delete a town if the town has no residents and all town residents have been given 8 days notice of the deletion.
* Depleting the town bank to cause the town to fall is not permitted.
* Town staff disputes will not be moderated.
* You may only have 25 hoppers per two chunks.

**No AFK or Auto-Farms**

* These would not help with your jobs anyway.
* Mob grinders with or without spawners are allowed, but the mobs must be killed by the player.
* Auto smelters are allowed but will not pay out for your job.

**No Griefing**

* Do not make your plot look unpleasant before leaving a town through any actions that would constitute grief.
* Do not kill animals, owned or unowned within a town, follow town farm rules.

**Town PvP Arenas**

* PvP arenas must have rules clearly written outside of the arena and the consequences for breaking those rules.
* Combat logging is not permitted within any arena.
    * Intentionally disconnecting from the server by any means during combat or any form of teleportation out of an arena is considered logging.
* SnowynMC staff will not intervene with the running of town PvP arenas.