# Starting SkyBlock
Create your island with `/is create` and choose one of the starting islands available.

You should now be on your island with a chest and tree.  
If not then you can go to your island with `/is go`.

You can also visit other players islands if they are public using `/is go [playername]`.  
Or you can view a list of everyone that has an island with `/is explore`.

View the top islands with `/is top`.  
View your island level with `/is level`.

### Island Border and Settings
You can toggle the border and even change the color with `/is border`.

You can change island settings such as perms and weather using `/is` and navigating through the different menus.

The perms can be changed very extensively for members and visitors.

By using `/is` then clicking the **Name Tag ⇢ Sapling** you can toggle things such as mob spawning and fire spread.

### Island Spawn
You can change your islands spawn for members or visitors using `/is setspawn`.

### Island Chat
Chat with other members of your island using `/is chat` to toggle it.

### The More The Merrier
If you have friends you can invite them using `/is invite [name]`.

Your friends then accept it using `/is accept [name]` or `/is deny [name]` if they are one of those people.

### How To Earn Money
The economy is a lot different from towny because it uses server shops.

You can purchase and sell items at the shops at `/spawn`.

You can sell your items to other players using Player Shops which is available with `/ps`.

To create or manage your player shop click the chest in the `/ps` menu.

To recieve your funds from player purchases you need to use `/ps` then click the **Chest ⇢ Map**.

**How do I get a player shop sign at spawn?**  
[Create a ticket](tickets) asking to have a sign placed at spawn.

### Ore Generator
Just by creating a cobble generator with lava and water there will be a chance that ores will spawn from it.

We plan on making this upgradeable in the future.

### Island Upgrades
Being able to purchase upgrades for your island is a big part of this.

See available upgrades with `/is upgrade`.

You can increase your border size up to 5 tiers.