# Trade Boxes
To help prevent scamming and make trading with other players fair for everyone we have made these trading boxes.

You can go down to the trading rooms by entering `/warp trade`.

<img src="https://i.ibb.co/XCgj3Ws/trade-box.jpg">

### How The Room Works
You stand on the wood part of the room and wait for the person you are trading with to enter the room.

You and the person you are trading with should stand on the pressure plates when ready to trade.

The door will remain closed until everyone has stepped off the pressure plates.

This prevents anyone from leaving the room until everyone has upheld their agreement.

### Commands Allowed
While in the room you may only use the following commands.

- **pay**
- **paytoggle**
- **payconfirm**
- **bal**
- **rules**
- **ticket**