# Confirm Minecraft Name

You must confirm your Minecraft name before you can chat or use voice on the Discord.

Your Discord nickname will automatically be updated to your Minecraft username when you have successfully confirmed.

**To confirm your Minecraft name..**
- [Join our discord server](https://discord.snowynmc.ga)
- While in the `#confirm-minecraft-name` channel enter the command `~confirm-mc`
- Then follow the instructions **SnowynBot** sends in the channel 